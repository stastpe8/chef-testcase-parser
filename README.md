# Chef testcase parser

This is part of the [Chef thesis](https://projects.fit.cvut.cz/theses/4754).

This is a library used by other tools that process results of chef.
The library is deployed to a gitlab registry of the [chef-nextgen](https://gitlab.fit.cvut.cz/stastpe8/chef-nextgen) repository that hosts rest of the artifacts
for this thesis as well.

The goal is to parse JSON output of Chef and generate a convenient format that can be used by other tools.

In order to use this library, get `chef-testcase-parser`.

```sh
pip install chef-testcase-parser --index-url https://gitlab.fit.cvut.cz/api/v4/projects/60767/packages/pypi/simple
```
