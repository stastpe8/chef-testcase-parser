from pathlib import Path

from chef_testcase_parser.r_types import RAssignment, RTestCase
from chef_testcase_parser.testcase import Assignment, TestCase, TestRun


def R_assignments_from_file(path: Path) -> list[RTestCase]:
    with path.open() as f:
        test_run = TestRun.from_json(f.read())

    return [RTestCase.from_test_case(test_case) for test_case in test_run.testcases]


__all__ = [
    "r_types",
    "RAssignment",
    "RTestCase",
    "R_assignments_from_file",
    "Assignment",
    "TestCase",
    "TestRun",
    "Path",
]
