from dataclasses import dataclass
from typing import Any, ClassVar, Tuple

from chef_testcase_parser.r_types.testcase import RAssignment
from chef_testcase_parser.testcase import Assignment


@dataclass
class AnyType(RAssignment):
    type_id: ClassVar[str] = "any"

    value: list[Any]

    @classmethod
    def can_parse(cls, assignments: list[Assignment]) -> bool:
        # First arg. has type any_type, second one has the same name as the first one.
        # Except if the first assignment has i32 value of 4. In this case, we are parsing a nil,
        # so there is no second assignment.
        if len(assignments) == 0:
            return False

        if not assignments[0].name.startswith(cls.type_id):
            return False

        if assignments[0].i32 in [4, 11, 12]:
            return True

        # We require at least two assignments for any other type
        if len(assignments) < 2:
            return False

        # Check whether the name is same. The type of the second assignment could be anything,
        # so we cannot use get_stripped_name easily.
        if (
            cls._get_stripped_name(assignments[0].name)
            != assignments[1].name.split("__")[1]
        ):
            return False

        # Verify whether the next assignment can be parsed
        from chef_testcase_parser.r_types import TYPES

        valid_classes = [cls for cls in TYPES if cls.can_parse(assignments[1:])]

        return len(valid_classes) == 1

    def get_literal(self) -> str:
        return f"{self.value}"

    @classmethod
    def from_testcase(cls, assignments: list[Assignment]) -> Tuple[RAssignment, int]:
        """
        Any is not a real type, but a placeholder for any type.
        One will be selected dynamically based on the test data and returned instead.
        """
        from chef_testcase_parser.r_types import TYPES
        from chef_testcase_parser.r_types.logical import Logical
        from chef_testcase_parser.r_types.nil import Nil

        if not cls.can_parse(assignments):
            raise ValueError("Any cannot be parsed from this.")

        assignment_type = assignments[0]
        name = cls._get_stripped_name(assignment_type.name)

        if assignment_type.i32 is None:
            raise ValueError(f"Unknown any type: {assignment_type.bytes.decode}")

        # We only care about the type if it is NIL, TRUE and FALSE, which are IDs 4, 11 and 12
        # All other types will be indicated by the name in the usual way anyway, so we don't need this.
        if assignment_type.i32 == 4:
            # A nil value was returned. There is no symbolic value generated for that,
            # since there is just one value of nil
            return Nil(name, assignment_type), 1
        elif assignment_type.i32 == 11:
            return Logical(name, assignment_type, True), 1
        elif assignment_type.i32 == 12:
            return Logical(name, assignment_type, False), 1

        # Select appropriate type for the next datapoint, instantiate it and return it
        valid_classes_for_following_data = [
            cls for cls in TYPES if cls.can_parse(assignments[1:])
        ]

        if len(valid_classes_for_following_data) != 1:
            raise ValueError(
                f"Could not determine type for any: {valid_classes_for_following_data}"
            )

        next_assignment, consumed = valid_classes_for_following_data[0].from_testcase(
            assignments[1:]
        )

        return next_assignment, consumed + 1
