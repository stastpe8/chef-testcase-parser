from dataclasses import dataclass
from typing import ClassVar, Tuple

from chef_testcase_parser.r_types.testcase import RAssignment
from chef_testcase_parser.testcase import Assignment


@dataclass
class Integer(RAssignment):
    type_id: ClassVar[str] = "int"

    value: int

    def get_literal(self) -> str:
        return f"{self.value}L"

    @classmethod
    def from_testcase(cls, assignments: list[Assignment]) -> Tuple["Integer", int]:
        if not cls.can_parse(assignments):
            raise ValueError("Integer cannot be parsed from this.")

        assignment = assignments[0]

        name = cls._get_stripped_name(assignment.name)
        val = assignment.i64 or assignment.i32

        if val is None:
            raise ValueError("Integer had no valid data")

        return Integer(name, assignment, val), 1
