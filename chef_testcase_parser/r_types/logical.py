from dataclasses import dataclass
from typing import ClassVar, Tuple

from chef_testcase_parser.r_types.testcase import RAssignment
from chef_testcase_parser.testcase import Assignment


@dataclass
class Logical(RAssignment):
    type_id: ClassVar[str] = "logical"

    value: bool

    def get_literal(self) -> str:
        if self.value:
            return "T"
        else:
            return "F"

    @classmethod
    def from_testcase(cls, assignments: list[Assignment]) -> Tuple["Logical", int]:
        if not cls.can_parse(assignments):
            raise ValueError("Integer cannot be parsed from this.")

        assignment = assignments[0]

        name = cls._get_stripped_name(assignment.name)
        val = assignment.i64 or assignment.i32

        if val is None:
            raise ValueError("Integer had no valid data")

        return Logical(name, assignment, val != 0), 1
