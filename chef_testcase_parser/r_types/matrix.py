import struct
from dataclasses import dataclass
from typing import ClassVar, Tuple

from chef_testcase_parser.r_types.testcase import RAssignment
from chef_testcase_parser.testcase import Assignment


@dataclass
class Matrix(RAssignment):
    type_id: ClassVar[str] = "matrix"

    nrow: int
    ncol: int
    values: list[float]

    @classmethod
    def can_parse(cls, assignments: list[Assignment]) -> bool:
        if len(assignments) < 1:
            return False

        if not assignments[0].name.startswith(cls.type_id):
            return False

        return True

    def get_literal(self) -> str:
        return "matrix(c({}), nrow={}, ncol={})".format(
            ", ".join([str(value) for value in self.values]), self.nrow, self.ncol
        )

    @classmethod
    def from_testcase(cls, assignments: list[Assignment]) -> Tuple["Matrix", int]:
        if not cls.can_parse(assignments):
            raise ValueError("Matrix cannot be parsed from this.")

        metadata = assignments[0].name.split("__")
        nrow = int(metadata[1])
        ncol = int(metadata[2])
        name = metadata[-1]

        # There are ncol * nrow doubles
        if len(assignments[0].bytes) != nrow * ncol * 8:
            raise ValueError(f"Matrix {name} has invalid data size")

        values = list(struct.unpack(f"<{nrow * ncol}d", assignments[0].bytes))

        return Matrix(name, assignments[0], nrow, ncol, values), 1
