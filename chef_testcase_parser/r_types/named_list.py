from dataclasses import dataclass
from typing import Any, ClassVar, Tuple

from chef_testcase_parser.r_types.testcase import RAssignment
from chef_testcase_parser.testcase import Assignment


@dataclass
class List(RAssignment):
    type_id: ClassVar[str] = "list"

    names_values: list[tuple[Any, Any]]
    """
    List of tuples containing name and value of the list.
    The first element of the tuple is always r_types.String
    (not expressed in type system due to circular dependency)
    The second element is some r_type
    """

    @classmethod
    def can_parse(cls, assignments: list[Assignment]) -> bool:
        if len(assignments) < 1:
            return False

        if not assignments[0].name.startswith(cls.type_id):
            return False

        return True

    def get_literal(self) -> str:
        literal = "list("
        args = [
            f"{name.get_literal()}={value.get_literal()}"
            for name, value in self.names_values
        ]
        literal += ", ".join(args)
        literal += ")"
        return literal

    @classmethod
    def from_testcase(cls, assignments: list[Assignment]) -> Tuple["List", int]:
        from chef_testcase_parser.r_types.string import String
        from chef_testcase_parser.r_types.vector import Vector

        if not cls.can_parse(assignments):
            raise ValueError("List cannot be parsed from this.")

        list_size = int(assignments[0].name.split("__")[1])

        # Read N vectors
        values = []
        assignments_parsed = 1
        for _ in range(list_size):
            vector, parsed = Vector.from_testcase(assignments[assignments_parsed:])
            values.append(vector)
            assignments_parsed += parsed

        # Read N names
        names = []
        for _ in range(list_size):
            string, parsed = String.from_testcase(assignments[assignments_parsed:])
            names.append(string)
            assignments_parsed += parsed

        name = cls._get_stripped_name(assignments[0].name)

        return List(name, assignments[0], list(zip(names, values))), assignments_parsed
