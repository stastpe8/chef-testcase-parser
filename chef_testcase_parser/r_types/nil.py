from dataclasses import dataclass
from typing import ClassVar, Tuple

from chef_testcase_parser.r_types.testcase import RAssignment
from chef_testcase_parser.testcase import Assignment


@dataclass
class Nil(RAssignment):
    type_id: ClassVar[str] = "nil"

    def get_literal(self) -> str:
        return "NULL"

    @classmethod
    def from_testcase(cls, assignments: list[Assignment]) -> Tuple["Nil", int]:
        if not cls.can_parse(assignments):
            raise ValueError("Integer cannot be parsed from this.")

        assignment = assignments[0]

        name = cls._get_stripped_name(assignment.name)

        return Nil(name, assignment), 1
