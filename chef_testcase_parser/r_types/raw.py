from dataclasses import dataclass
from typing import ClassVar, Tuple

from chef_testcase_parser.r_types.testcase import RAssignment
from chef_testcase_parser.testcase import Assignment


@dataclass
class Raw(RAssignment):
    type_id: ClassVar[str] = "raw"

    value: bytes

    def get_literal(self) -> str:
        return f"as.raw(c({', '.join([str(byte) for byte in self.value])}))"

    @classmethod
    def from_testcase(cls, assignments: list[Assignment]) -> Tuple["Raw", int]:
        if not cls.can_parse(assignments):
            raise ValueError("Raw cannot be parsed from this.")

        assignment = assignments[0]

        name = cls._get_stripped_name(assignment.name)
        val = assignment.bytes

        if val is None:
            raise ValueError("Raw had no valid data")

        return Raw(name, assignment, val), 1
