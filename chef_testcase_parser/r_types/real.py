import struct
from dataclasses import dataclass
from typing import ClassVar, Tuple

from chef_testcase_parser.r_types.testcase import RAssignment
from chef_testcase_parser.testcase import Assignment


@dataclass
class Real(RAssignment):
    type_id: ClassVar[str] = "real"

    value: float

    def get_literal(self) -> str:
        return f"{self.value}"

    @classmethod
    def from_testcase(cls, assignments: list[Assignment]) -> Tuple["Real", int]:
        if not cls.can_parse(assignments):
            raise ValueError("Real cannot be parsed from this.")

        assignment = assignments[0]

        name = cls._get_stripped_name(assignment.name)
        val = assignment.bytes

        if val is None:
            raise ValueError("Integer had no valid data")

        return Real(name, assignment, struct.unpack("<d", val)[0]), 1
