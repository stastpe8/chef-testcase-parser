from dataclasses import dataclass
from typing import ClassVar, Tuple

from chef_testcase_parser.r_types.testcase import RAssignment
from chef_testcase_parser.testcase import Assignment


@dataclass
class String(RAssignment):
    type_id: ClassVar[str] = "str"

    value: str

    def get_literal(self) -> str:
        return f'"{self.value}"'

    @classmethod
    def from_testcase(cls, assignments: list[Assignment]) -> Tuple["String", int]:
        if not cls.can_parse(assignments):
            raise ValueError("String cannot be parsed from this.")

        assignment = assignments[0]

        name = cls._get_stripped_name(assignment.name)
        val = assignment.bytes

        if val is None:
            raise ValueError("String had no valid data")

        # We want to decode bytes rather than use the string, it's mostly for the user here.
        # The reason is correct decoding of special escaped bytes, which, due to json spec, need to be
        # written out as \x10 or so (note that the \ does not represent an escape character here, but a literal
        # character). Parsing from bytes is just easier.
        # There might be some invalid characters regardless that R will not accept in a literal,
        # not sure what to do about that.
        string = val.decode("utf-8", errors="replace")

        return String(name, assignment, string), 1
