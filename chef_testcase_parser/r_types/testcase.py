import logging
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import ClassVar, Sequence, Tuple

from chef_testcase_parser.testcase import Assignment, TestCase


@dataclass
class RAssignment(ABC):
    type_id: ClassVar[str]
    """
    Type that the given assignment is able to parse. This is used in the can_parse method by default.
    """

    name: str
    """
    Name of variable, not including type metadata
    """
    chef_assignment: Assignment
    """
    Raw assignment underneath that this was generated from. Might not correspond 1:1 to this,
    but contains metadata about this assignment.
    """

    @classmethod
    def can_parse(cls, assignments: list[Assignment]) -> bool:
        return len(assignments) >= 1 and assignments[0].name.startswith(cls.type_id)

    @classmethod
    @abstractmethod
    def from_testcase(cls, assignments: list[Assignment]) -> Tuple["RAssignment", int]:
        ...

    """
    Parse specific R data from assignments.
    Takes a list of assignments and returns the number of them that were parsed.
    :return: (R assignment, number of assignments that were successfully parsed)
    """

    @abstractmethod
    def get_literal(self) -> str:
        ...

    def get_type_name(self) -> str:
        return self.type_id

    @classmethod
    def _get_stripped_name(cls, name: str, prefix: str | None = None) -> str:
        if prefix is None:
            prefix = cls.type_id

        prefix += "__"

        if not name.startswith(prefix):
            raise ValueError(f"Unknown type identifier: {name}")

        # The name contains name of the type (and potentially some other data
        # delimited by __). Remove it.
        return name.split("__")[-1]


@dataclass
class RTestCase:
    assignments: Sequence[RAssignment]
    metadata: TestCase

    @classmethod
    def from_test_case(cls, test_case: TestCase) -> "RTestCase":
        from chef_testcase_parser.r_types import TYPES

        assignments: list[RAssignment] = []

        offset = 0
        while offset < len(test_case.inputs):
            # Get a class that can handle head of the assignments
            inputs = test_case.inputs[offset:]

            valid_classes = [cls for cls in TYPES if cls.can_parse(inputs)]

            if len(valid_classes) == 0:
                logging.info(f"No class can parse: {inputs[0]}. Ignoring.")
                offset += 1
                continue

            assert (
                len(valid_classes) == 1
            ), f"There is not a single class that can parse: {inputs[0]}, notably: {valid_classes}"

            class_to_parse = valid_classes[0]

            r_assignment, parsed = class_to_parse.from_testcase(inputs)

            offset += parsed
            assignments.append(r_assignment)

        return cls(assignments=assignments, metadata=test_case)
