import struct
from dataclasses import dataclass
from enum import IntEnum
from typing import Any, ClassVar, Tuple

from chef_testcase_parser.r_types.testcase import RAssignment
from chef_testcase_parser.testcase import Assignment


class VectorDataType(IntEnum):
    """
    This corresponds to the type of SEXPTYPEs in R.
    See https://cran.r-project.org/doc/manuals/r-release/R-ints.html#SEXPs
    """

    LOGICAL = 10
    """
    LGLSXP (boolean)
    """
    INTEGER = 13
    """
    INTSXP (int)
    """
    DOUBLE = 14
    """
    REALSXP (float)
    """
    COMPLEX = 15
    """
    CPLXSXP (complex)
    """
    RAW = 24
    """
    RAWSXP (bytes)
    """


def _parse_vector_data(
    data: bytes, data_type: VectorDataType
) -> list[complex] | tuple[Any, ...]:
    # See https://docs.python.org/3.10/library/struct.html#struct.unpack for more info,
    # including the sizes of data
    if data_type == VectorDataType.LOGICAL:
        return struct.unpack(f"<{len(data)}?", data)
    elif data_type == VectorDataType.INTEGER:
        return struct.unpack(f"<{len(data)//4}i", data)
    elif data_type == VectorDataType.DOUBLE:
        return struct.unpack(f"<{len(data)//8}d", data)
    elif data_type == VectorDataType.COMPLEX:
        # Complex in R consists of two doubles, first being rez, the second one being imz
        doubles = struct.unpack(f"<{len(data)//8}d", data)
        # This loaded all doubles, let's make sure there's an even number of them
        if len(doubles) % 2 != 0:
            raise ValueError("Complex vector must have an even number of doubles.")
        # Turn them into a complex number
        return [complex(doubles[i], doubles[i + 1]) for i in range(0, len(doubles), 2)]
    elif data_type == VectorDataType.RAW:
        return struct.unpack(f"<{len(data)}B", data)
    else:
        raise ValueError(f"Unknown vector data type: {data_type}")


@dataclass
class Vector(RAssignment):
    type_id: ClassVar[str] = "vec_type"

    data_type: VectorDataType
    value: list[Any]

    @classmethod
    def can_parse(cls, assignments: list[Assignment]) -> bool:
        # First arg. has type vec_type, second arg. has type vec_value and both have the same variable name
        return (
            len(assignments) >= 2
            and assignments[0].name.startswith(cls.type_id)
            and assignments[1].name.startswith("vec_data")
            and cls._get_stripped_name(assignments[0].name)
            == cls._get_stripped_name(assignments[1].name, "vec_data")
        )

    def _literal_for_single_value(self, value: Any) -> str:
        if self.data_type == VectorDataType.LOGICAL:
            return "T" if value else "F"
        elif self.data_type == VectorDataType.INTEGER:
            return f"{value}L"
        elif self.data_type == VectorDataType.DOUBLE:
            return f"{value}"
        elif self.data_type == VectorDataType.COMPLEX:
            return f"{value.real}+{value.imag}i"
        elif self.data_type == VectorDataType.RAW:
            return f"{value}"
        else:
            raise ValueError(f"Unknown vector data type: {self.data_type}")

    def get_literal(self) -> str:
        if self.data_type == VectorDataType.RAW:
            # We need to wrap the vector into as.raw
            return f"c(as.raw(c({', '.join([self._literal_for_single_value(value) for value in self.value])})))"
        else:
            return f"c({', '.join([self._literal_for_single_value(value) for value in self.value])})"

    def get_type_name(self) -> str:
        return f"vec<{self.data_type.name.lower()}>"

    @classmethod
    def from_testcase(cls, assignments: list[Assignment]) -> Tuple["Vector", int]:
        if not cls.can_parse(assignments):
            raise ValueError("Vector cannot be parsed from this.")

        assignment_type = assignments[0]
        assignment_data = assignments[1]

        if assignment_type.i32 is None:
            raise ValueError(f"Unknown vector type: {assignment_type.bytes.decode}")

        name = cls._get_stripped_name(assignment_type.name)
        data_type = VectorDataType(assignment_type.i32)
        value = _parse_vector_data(assignment_data.bytes, data_type)

        return Vector(name, assignment_type, data_type, list(value)), 2
