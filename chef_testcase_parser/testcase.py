from dataclasses import dataclass
from datetime import timedelta
from typing import Sequence

from pydantic import BaseModel, Field, TypeAdapter, model_validator


class Assignment(BaseModel):
    name: str
    bytes: bytes
    i32: int | None = None
    i64: int | None = None
    string: str

    @model_validator(mode="after")
    def _validate_names(self) -> "Assignment":
        """
        Names that are outputed by chef are in format like
        v0_[NAME]_0, v1_[NAME]_1, etc.

        We do not care about the numbers, we just want the name
        """
        self.name = "_".join(self.name.split("_")[1:-1])
        return self

    @model_validator(mode="after")
    def _validate_bytes(self) -> "Assignment":
        """
        Bytes are loaded as hex string, such as "0xbf 0xff" and so on,
        we want to parse it into actual bytes
        """
        if len(self.bytes) > 0:
            hexes = self.bytes.decode("utf-8").split(" ")
            self.bytes = bytes([int(h, 16) for h in hexes])
        else:
            self.bytes = b""

        return self


class TestCase(BaseModel):
    timestamp: timedelta
    pc: int
    state_id: int = Field(alias="stateId")
    inputs: list[Assignment]


@dataclass
class TestRun:
    testcases: Sequence[TestCase]

    @classmethod
    def from_json(cls, json: str) -> "TestRun":
        # In some cases, we might receive a malformed JSON.
        # This is because when s2e is killed and does not explore
        # the whole space, it does not write the trailing ']' to
        # properly end to JSON array.
        # If this happens, we don't mind - we just add the ']' to
        # the end of the string and continue.
        if not json.strip().endswith("]"):
            json += "]"

        return cls(testcases=TypeAdapter(list[TestCase]).validate_json(json))
