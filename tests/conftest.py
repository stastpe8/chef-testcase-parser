from pathlib import Path

import pytest

from chef_testcase_parser import R_assignments_from_file, RTestCase


def load_file(filename: str) -> list[RTestCase]:
    path = Path(__file__).parent / "data" / filename
    return R_assignments_from_file(path)


@pytest.fixture(scope="module")
def testrun_all_types():
    return load_file("all_types.json")


@pytest.fixture(scope="module")
def testrun_any_missing_parenthesis():
    return load_file("any_missing_trailing_paren.json")


@pytest.fixture(scope="module")
def testrun_vectors():
    return load_file("vectors.json")


@pytest.fixture(scope="module")
def testrun_list():
    return load_file("list.json")


@pytest.fixture(scope="module")
def testrun_matrix():
    return load_file("matrix.json")


@pytest.fixture(scope="module")
def testrun_any_logical():
    return load_file("any_logical.json")
