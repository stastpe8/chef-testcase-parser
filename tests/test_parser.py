import pytest

import chef_testcase_parser
from tests.conftest import load_file


def test_multiple_colliding_types(monkeypatch):
    monkeypatch.setattr(
        chef_testcase_parser.r_types,
        "TYPES",
        [
            chef_testcase_parser.r_types.Integer,
            chef_testcase_parser.r_types.Real,
            chef_testcase_parser.r_types.Vector,
            chef_testcase_parser.r_types.Real,  # two types with the same prefix
        ],
    )

    with pytest.raises(AssertionError):
        # Errors out
        load_file("all_types.json")


def test_parsing_missing_trailing_parenthesis(testrun_any_missing_parenthesis):
    # Here, assignment already succeeded
    assert len(testrun_any_missing_parenthesis) == 5
    # Check whether everything that was valid was loaded. There are some that are invalid.
    assert (
        sum(len(testcase.assignments) for testcase in testrun_any_missing_parenthesis)
        == 3
    )
