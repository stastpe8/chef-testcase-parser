import pytest

from chef_testcase_parser.r_types import Integer, List, Matrix, Real, Vector
from chef_testcase_parser.r_types.nil import Nil
from chef_testcase_parser.r_types.vector import VectorDataType


def test_int(testrun_all_types) -> None:
    assignment: Integer = testrun_all_types[0].assignments[0]

    assert assignment.name == "foo"
    assert assignment.value == 21913


def test_real(testrun_all_types) -> None:
    assignment: Real = testrun_all_types[1].assignments[0]

    assert assignment.name == "bar"
    assert assignment.value == 42


def test_vector(testrun_all_types) -> None:
    assignment: Vector = testrun_all_types[2].assignments[0]

    assert assignment.name == "baz"
    assert assignment.data_type == VectorDataType.LOGICAL
    assert assignment.value == [
        True,
        True,
        True,
        False,
        True,
        True,
        True,
        False,
        True,
        True,
        True,
        False,
        True,
        True,
        True,
        False,
        True,
        True,
        True,
        False,
    ]


def test_any(testrun_all_types) -> None:
    any_vector: Vector = testrun_all_types[3].assignments[0]

    assert isinstance(any_vector, Vector)

    assert any_vector.name == "quid"
    assert any_vector.data_type == VectorDataType.LOGICAL
    assert any_vector.value == [True, True]


def test_any_nil(testrun_all_types) -> None:
    any_nil: Nil = testrun_all_types[4].assignments[0]

    assert isinstance(any_nil, Nil)

    assert any_nil.name == "nil"


def test_type_raw(testrun_all_types) -> None:
    assignment = testrun_all_types[5].assignments[0]

    assert assignment.name == "xz"
    assert list(assignment.value) == [
        0x66,
        0x72,
        0x69,
        0x65,
        0x6E,
        0x64,
        0x73,
        0x68,
        0x69,
        0x70,
        0x20,
        0x69,
        0x73,
        0x20,
        0x6D,
        0x61,
        0x67,
        0x69,
        0x63,
    ]


def test_type_string(testrun_all_types) -> None:
    assignment = testrun_all_types[6].assignments[0]

    assert assignment.name == "quaz"
    assert assignment.value == "Newlines\nand escapes\x10work as well"


@pytest.mark.parametrize(
    "idx_to_test,expected_type,expected_outcome,expected_literal",
    [
        (0, VectorDataType.LOGICAL, [True, False, True, False], "c(T, F, T, F)"),
        (1, VectorDataType.INTEGER, [10, 11, 12], "c(10L, 11L, 12L)"),
        (2, VectorDataType.DOUBLE, [42, 43], "c(42.0, 43.0)"),
        (3, VectorDataType.COMPLEX, [42 + 43j, 42 + 43j], "c(42.0+43.0i, 42.0+43.0i)"),
        (
            4,
            VectorDataType.RAW,
            [1, 2, 3, 4, 5, 6, 7],
            "c(as.raw(c(1, 2, 3, 4, 5, 6, 7)))",
        ),
        (5, VectorDataType.LOGICAL, [], "c()"),
        (6, VectorDataType.INTEGER, [], "c()"),
    ],
)
def test_all_vector_types(
    testrun_vectors,
    idx_to_test: int,
    expected_type: VectorDataType,
    expected_outcome: list,
    expected_literal: str,
) -> None:
    assignment: Vector = testrun_vectors[idx_to_test].assignments[0]

    assert assignment.data_type == expected_type
    assert assignment.value == expected_outcome
    assert assignment.get_literal() == expected_literal


def test_list(testrun_list) -> None:
    assignment: List = testrun_list[0].assignments[0]

    assert len(testrun_list[0].assignments) == 1  # one element: a list
    assert (
        len(assignment.names_values) == 5
    )  # 5 elements in the list, multiple duped names are ok
    assert assignment.name == "xxx"


def test_matrix(testrun_matrix) -> None:
    assignment: Matrix = testrun_matrix[0].assignments[0]

    assert len(testrun_matrix[0].assignments) == 1
    assert len(assignment.values) == 5 * 3
    assert assignment.nrow == 5
    assert assignment.ncol == 3
    assert assignment.name == "matr"


def test_any_logical(testrun_any_logical) -> None:
    true = testrun_any_logical[0].assignments
    false = testrun_any_logical[1].assignments

    assert true[0].name == "foo"
    assert true[0].get_literal() == "T"

    assert false[0].name == "bar"
    assert false[0].get_literal() == "F"
